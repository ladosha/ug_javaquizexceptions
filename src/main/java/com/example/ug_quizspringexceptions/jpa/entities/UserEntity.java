package com.example.ug_quizspringexceptions.jpa.entities;


import com.example.ug_quizspringexceptions.dto.UserDto;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode()
@Table(name = "USERS")
public class UserEntity {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "userIdSeq", sequenceName = "USER_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userIdSeq")
    private Long id;

    @Column(name = "EMAIL")
    private String email;

    //password is stored as plaintext, in real apps we should write
    //getter and setter which would wrap it in a hash function
    @Column(name = "PASSWORD")
    private String password;

    public UserEntity(UserDto userDto) {
        this.email = userDto.getUserEmail();
        this.password = userDto.getUserPassword();
    }
}
