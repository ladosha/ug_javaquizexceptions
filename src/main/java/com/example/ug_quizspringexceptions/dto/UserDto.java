package com.example.ug_quizspringexceptions.dto;

import lombok.Data;

@Data
public class UserDto {
    private Long userId;
    private String userEmail;
    private String userPassword;
}
