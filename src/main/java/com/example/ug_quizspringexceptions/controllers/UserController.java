package com.example.ug_quizspringexceptions.controllers;

import com.example.ug_quizspringexceptions.dto.ApiResponse;
import com.example.ug_quizspringexceptions.dto.UserDto;
import com.example.ug_quizspringexceptions.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    private final UserService departmentService;

    @Autowired
    public UserController(UserService departmentService) {
        this.departmentService = departmentService;
    }

    @PostMapping("/user/add")
    public ApiResponse addDepartment(@RequestBody UserDto userDto) {
        return departmentService.addUser(userDto);
    }
}
