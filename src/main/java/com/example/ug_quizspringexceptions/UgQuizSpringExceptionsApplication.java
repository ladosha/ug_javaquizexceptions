package com.example.ug_quizspringexceptions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UgQuizSpringExceptionsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UgQuizSpringExceptionsApplication.class, args);
    }

}
