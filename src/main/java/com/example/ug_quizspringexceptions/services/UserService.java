package com.example.ug_quizspringexceptions.services;

import com.example.ug_quizspringexceptions.apiUtils.ApiUtils;
import com.example.ug_quizspringexceptions.apiUtils.IncorrectParameterException;
import com.example.ug_quizspringexceptions.apiUtils.UserAlreadyExistsException;
import com.example.ug_quizspringexceptions.dto.ApiResponse;
import com.example.ug_quizspringexceptions.dto.UserDto;
import com.example.ug_quizspringexceptions.jpa.entities.UserEntity;
import com.example.ug_quizspringexceptions.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public ApiResponse addUser(UserDto userDto) {

        if(userDto == null || userDto.getUserEmail() == null ||
        userDto.getUserEmail().isEmpty() || userDto.getUserPassword() == null ||
                userDto.getUserPassword().isEmpty()) {
            throw new IncorrectParameterException().addIncorrectParameter("user");
        }

        if(userRepository.findAllByEmail(userDto.getUserEmail()).size() > 0) {
            throw new UserAlreadyExistsException().setProblemEmail(userDto.getUserEmail());
        }

        UserEntity user = userRepository.save(new UserEntity(userDto));

        return ApiUtils.getApiResponse(user);
    }
}
